Приложение для ведения бюджета. 
Позволяет удобно и наглядно увидеть на что вы тратите деньги.

Функционал:
- ввод/удаление расходов
- создание/удаление категорий
- установка бюджета по всем категориям
- установка бюджета по конкретныи категориям
- вывод статистики расходов в ввиде круговой диаграммы
- просмтр превышений по созданным бюджетам
- просмотр статистики расходов за разные промежутки времени

# TODO вывод статистики в виде графика
# TODO выгрузка расходов в exel файл