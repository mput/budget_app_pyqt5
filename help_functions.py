import datetime
from PyQt5.QtWidgets import QMessageBox

def to_datetime(date: str):
    date_lst = date.split('-')
    year = int(date_lst[0])
    month = int(date_lst[1]) if date_lst[1][0] != '0' else int(date_lst[1][-1])
    day = int(date_lst[2]) if date_lst[2][0] != '0' else int(date_lst[2][-1])
    return datetime.date(year, month, day)


def get_begin_of_date(date, date_range):
    if date_range == 'Неделя':
        delta = datetime.timedelta(days=6 - datetime.date.weekday(date))
        return date - delta
    elif date_range == 'Месяц':
        return datetime.date(date.year, date.month, 1)
    elif date_range == 'Год':
        return datetime.date(date.year, 1, 1)
    elif date_range == 'День':
        return date
    elif date_range == 'За все время':
        return datetime.date(1, 1, 1)

def mistake_messege(text):
    msg_box = QMessageBox()
    msg_box.setIcon(QMessageBox.Information)
    msg_box.setText(text)
    msg_box.setWindowTitle("Ошибка")
    msg_box.setStandardButtons(QMessageBox.Ok)

    msg_box.exec()
