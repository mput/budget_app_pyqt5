import datetime
import os
import sys
from pathlib import Path

from PyQt5 import uic
from PyQt5.QtCore import QUrl
from PyQt5.QtWebEngineWidgets import QWebEngineView
from PyQt5.QtWidgets import QApplication, QMainWindow, QPushButton, QLabel, QComboBox, QButtonGroup, QDateEdit, \
    QLineEdit, QTableWidget, QTableWidgetItem, QMessageBox
from plotly.express import pie

from category_edit import CategoryEditWindow
from data_base import Database
from goal import GoalWidget
from goal_analise import GoalAnaliseWidget
from help_functions import get_begin_of_date, mistake_messege


class MainWidget(QMainWindow):
    create: QPushButton
    history_of_expenses: QTableWidget
    diagram: QLabel
    edit_categories: QPushButton
    goal_creating: QPushButton
    time_range_choosing: QButtonGroup
    dateEdit: QDateEdit
    choose_category: QComboBox
    feedback: QLabel
    amount: QLineEdit
    count_day: QDateEdit
    delete_chosen_elements: QPushButton
    browser: QWebEngineView
    sm: QLabel
    goal_analise: QPushButton

    def __init__(self):
        super().__init__()
        self.db = Database('money_data.db')
        uic.loadUi('uis/main.ui', self)
        self.time_range = None
        self.dateEdit.setDate(datetime.date.today())
        self.count_day.setDate(datetime.date.today())
        self.time_range_choosing.buttons()[-1].setChecked(True)
        self.category_choosing()
        self.show_pie_diagram()
        self.create.clicked.connect(self.create_expense)
        self.add_elements_in_table(self.count_day.date().toPyDate())
        self.delete_chosen_elements.clicked.connect(self.delete_elements)
        self.time_range_choosing.buttonClicked.connect(self.show_pie_diagram)
        self.edit_categories.clicked.connect(self.show_category_edit_window)
        self.goal_creating.clicked.connect(self.show_goal_widget)
        self.goal_analise.clicked.connect(self.show_goal_analise_widget)

    def category_choosing(self):
        self.choose_category.clear()
        for el in self.db.get_categories():
            self.choose_category.addItem(str(el))

    def get_home_directory(self):
        diagram_folder = Path.home() / 'data'
        if not diagram_folder.exists():
            diagram_folder.mkdir()
        diagram_name = diagram_folder / 'plot.html'
        return str(diagram_name)

    def create_expense(self):
        date = self.dateEdit.date().toPyDate()
        category = self.choose_category.currentText()
        amount = self.amount.text()
        if amount.isdigit():
            self.db.add_expense(category, amount, date)
            self.show_pie_diagram()
        elif amount == '':
            mistake_messege('Введите сумму покпуки.')
        else:
            mistake_messege("В сумме присутствуют посторонние символы. Используйте только цифры при вводе.")
        self.add_elements_in_table(self.count_day.date().toPyDate())

    def add_elements_in_table(self, date):
        self.history_of_expenses.clear()
        result = self.db.get_elements_with(get_begin_of_date(date, self.time_range), date)
        self.history_of_expenses.setColumnCount(4)
        self.history_of_expenses.setRowCount(len(result))
        for i, elem in enumerate(result[::-1]):
            rows = elem.get_all_data()[1:]
            rows.append(elem.get_all_data()[0])
            for j, val in enumerate(rows):
                self.history_of_expenses.setItem(i, j, QTableWidgetItem(str(val)))

    def show_pie_diagram(self):
        self.time_range = self.time_range_choosing.checkedButton().text()
        self.add_elements_in_table(self.count_day.date().toPyDate())
        categories = self.db.get_categories()
        date = self.count_day.date().toPyDate()
        self.collect_elements(categories, date)

    def collect_elements(self, categories, date):
        amounts = []
        for category in categories:
                amounts.append(sum([int(i.amount) for i in
                                    self.db.get_elements_with(get_begin_of_date(date, self.time_range),
                                                              date, category) if i.category == category]))
        self.sm.setText(f'Потрачено: {sum(amounts)}')
        while 0 in amounts:
            ind = amounts.index(0)
            categories.pop(ind)
            amounts.pop(ind)

        self.create_diagram(amounts, categories)

    def create_diagram(self, values, names):
        if len(values) != 0:
            html = '<html><body>'
            plot = pie(values=values, names=names)
            plot.write_html(f"{os.getcwd()}/plot.html")
            html += '</body></html>'
            local_url = QUrl.fromLocalFile(f"{os.getcwd()}/plot.html")
            self.browser.load(local_url)

    def delete_elements(self):
        rows = list(set([i.row() for i in self.history_of_expenses.selectedItems()]))
        ids = [self.history_of_expenses.item(i, 3).text() for i in rows]
        self.db.remove_expense(*ids)
        self.add_elements_in_table(self.count_day.date().toPyDate())
        self.show_pie_diagram()

    def show_category_edit_window(self):
        window = CategoryEditWindow(self)
        window.show()

    def show_goal_widget(self):
        window = GoalWidget(self)
        window.show()

    def make_goal_analise(self):
        date = self.count_day.date().toPyDate()
        categories = self.db.get_categories()
        amounts = {}
        lst = []
        if self.time_range == 'За все время':
            data = self.db.get_expenses()
            for category in categories:
                amounts[category] = sum([int(i.amount) for i in data if i.category == category])
        else:
            for category in categories:
                amounts[category] = sum([int(i.amount) for i in
                                         self.db.get_elements_with(get_begin_of_date(date, self.time_range),
                                                                   date, category)])
        goals = self.db.get_category_goals()
        feedback = []
        for goal in goals:
            if amounts.get(goal.category) is not None:
                if amounts[goal.category] > int(goal.amount):
                    feedback.append(
                        f'Вы превысили бюджет по категории "{goal.category}" на  {amounts[goal.category] - int(goal.amount)}'
                    )

        total = sum([amounts[x] for x in amounts])
        if self.db.get_all_goal() < total:
            feedback.append(f'Вы превысили бюджет на все покупки на {total - int(self.db.get_all_goal())}')

        return feedback

    def show_goal_analise_widget(self):
        window = GoalAnaliseWidget(self.make_goal_analise(), self)
        window.show()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = MainWidget()
    ex.show()
    sys.exit(app.exec_())
