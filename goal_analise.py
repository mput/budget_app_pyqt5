from PyQt5 import uic
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QMainWindow, QTextBrowser, QLabel, QPushButton

from data_base import Database


class GoalAnaliseWidget(QMainWindow):
    textBrowser: QTextBrowser
    label: QLabel

    def __init__(self, strings, parent=None):
        super().__init__(parent)
        self.db = Database('money_data.db')
        uic.loadUi('uis/goal_analise.ui', self)
        self.textBrowser.setText('\n'.join(strings))
        if len(strings) != 0:
            self.pixmap = QPixmap('pics/not_stonks.png')
            self.label.setPixmap(self.pixmap)
        else:
            self.pixmap = QPixmap('pics/stonks.png')
            self.textBrowser.setText('Вы соблюдаете все планы! Круто!')
            self.label.setPixmap(self.pixmap)

