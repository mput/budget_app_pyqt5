import sqlite3
from typing import List

# scripts = {
#     'create expenses table': """create table if not exists expenses
# (
#     id       INTEGER PRIMARY KEY AUTOINCREMENT
#                      UNIQUE
#                      NOT NULL,
#     date     DATE,
#     amount   REAL    NOT NULL,
#     category STRING
# );
#     """,
#     'create expense_categories table': """CREATE TABLE expense_categories (
#     id       INTEGER PRIMARY KEY AUTOINCREMENT
#                      NOT NULL,
#     category STRING  UNIQUE ON CONFLICT IGNORE
# );"""
# }
con = sqlite3.connect('money_data.db')
con.execute('''CREATE TABLE IF NOT EXISTS expense_categories (
    id       INTEGER PRIMARY KEY AUTOINCREMENT
                     NOT NULL,
    category STRING  UNIQUE ON CONFLICT IGNORE
);''')
con.execute("""create table if not exists expenses
(
    id       INTEGER PRIMARY KEY AUTOINCREMENT
                     UNIQUE
                     NOT NULL,
    date     DATE,
    amount   REAL    NOT NULL,
    category STRING
);
    """)
con.execute("""CREATE TABLE IF NOT EXISTS goals (
    id       INTEGER PRIMARY KEY AUTOINCREMENT
                     UNIQUE
                     NOT NULL,
    amount   INTEGER,
    category STRING  UNIQUE ON CONFLICT REPLACE
);
    """)


class Expense:
    def __init__(
            self,
            id: int,
            date: str,
            amount: float,
            category: str
    ):
        self.date: str = date
        self.amount = amount
        self.category = category
        self.id = id

    def get_all_data(self) -> list:
        lst = []
        lst.append(self.id)
        lst.append(self.date)
        lst.append(self.amount)
        lst.append(self.category)
        return lst


class ExpenseCategory:
    def __init__(
            self,
            id: int,
            category: str
    ):
        self.category = category
        self.id = id


class Goal:
    def __init__(self,
                 amount: int,
                 category: str
                 ):
        self.id = id
        self.amount = amount
        self.category = category


class Database:
    def __init__(self, db_name: str):
        self.db_name = db_name
        self._conn = sqlite3.connect(db_name)
        self.main_categories = ['супермаркеты', 'питомец', 'лекарства', 'жилье', 'одежда', 'машина',
                                'комунальные платежи',
                                'налоги']
        self.add_main_categories()

    def add_main_categories(self):
        for el in self.main_categories:
            self.add_category(el)

    def create_database(self):
        pass
        # for script in scripts.values():
        #     self._conn.execute(script)
        # self._conn.commit()

    def cursor(self) -> sqlite3.Cursor:
        return self._conn.cursor()

    def get_expenses(self) -> List[Expense]:
        cursor = self.cursor()
        values = cursor.execute("SELECT id,date,amount,category FROM expenses ORDER BY date DESC")
        result = [Expense(*i) for i in values]

        return result

    def get_categories(self) -> List[str]:
        cursor = self.cursor()
        values = cursor.execute("SELECT id,category FROM expense_categories")
        result = [ExpenseCategory(*i).category for i in values]
        return result

    def add_expense(self, category: str, amount: int, date: str):
        self._conn.execute(
            "INSERT INTO expenses (date, amount, category) VALUES (?,?,?)",
            (date, amount, category)
        )
        self._conn.commit()

    def add_category(self, category: str):
        self._conn.execute(
            "INSERT INTO expense_categories (category) VALUES (?)",
            (category,)
        )
        self._conn.commit()

    def clear_expenses(self):
        self._conn.execute("DELETE from expenses")
        self._conn.commit()

    def remove_expense(self, *ids):
        for id in ids:
            self._conn.execute("DELETE from expenses where id = ?",
                               (id,))
            self._conn.commit()

    def remove_category(self, *categories):
        for i in categories:
            self._conn.execute("DELETE from expense_categories where category = ?",
                               (i,))
            self._conn.commit()

    def get_elements_with(self, category, start_date, end_date):
        cursor = self.cursor()
        values = cursor.execute(
            f"SELECT id,date,amount,category FROM expenses where category like ? and date >= ? and date <= ?",
            (category, start_date, end_date))
        result = [Expense(*i) for i in values]
        return result

    def add_goal(self, amount, category):
        self._conn.execute('INSERT INTO goals (amount, category) VALUES (?, ?)',
                           (amount, category))
        self._conn.commit()

    def get_all_goal(self):
        cursor = self.cursor()
        values = cursor.execute('SELECT amount FROM goals where category = "any"')
        result = [i for i in values]
        if len(result) != 0:
            return result[0][0]
        else:
            return 99999999999999

    def get_category_goals(self):
        cursor = self.cursor()
        values = cursor.execute('SELECT amount, category FROM goals where category != "any"')
        result = [Goal(*i) for i in values]
        return result

    def close(self):
        self._conn.close()
