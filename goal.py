from PyQt5 import uic
from PyQt5.QtWidgets import QMainWindow, QPushButton, QLineEdit, QComboBox, QMessageBox
from help_functions import mistake_messege
from data_base import Database


class GoalWidget(QMainWindow):
    amount_of_all: QLineEdit
    create_month_budget: QPushButton
    amount_of_category: QLineEdit
    choose_category: QComboBox
    create_category_budget: QPushButton

    def __init__(self, parent=None):
        super().__init__(parent)
        self.db = Database('money_data.db')
        uic.loadUi('uis/goal_widget.ui', self)
        self.fill_combo_box()
        self.create_month_budget.clicked.connect(self.create_all_budget)
        self.create_category_budget.clicked.connect(self.set_category_budget)

    def fill_combo_box(self):
        self.choose_category.clear()
        for el in self.db.get_categories():
            self.choose_category.addItem(el)

    def set_category_budget(self):
        category_budget_goal = self.amount_of_category.text()
        category = self.choose_category.currentText()
        if category_budget_goal.isdigit():
            self.db.add_goal(category_budget_goal, category)
        elif category_budget_goal == '':
            mistake_messege("Введите сумму бюджета.")
        else:
            mistake_messege("В сумме присутствуют посторонние символы. Используйте только цифры при вводе.")

    def create_all_budget(self):
        all_budget_goal = self.amount_of_all.text()
        if all_budget_goal.isdigit():
            self.db.add_goal(all_budget_goal, 'any')
        elif all_budget_goal == '':
            mistake_messege("Введите сумму бюджета.")
        else:
            mistake_messege("В сумме присутствуют посторонние символы. Используйте только цифры при вводе.")



