from PyQt5 import uic
from PyQt5.QtWidgets import QMainWindow, QPushButton, QLineEdit, QTableWidget, QTableWidgetItem, QMessageBox
from data_base import Database
from help_functions import mistake_messege

class CategoryEditWindow(QMainWindow):
    category_table: QTableWidget
    delete_selected_categories: QPushButton
    name_of_category: QLineEdit
    add_category: QPushButton

    def __init__(self, parent=None):
        super().__init__(parent)
        self.parent = parent
        self.db = Database('money_data.db')
        uic.loadUi('uis/category_redactor.ui', self)
        self.add_category.clicked.connect(self.create_category)
        self.delete_selected_categories.clicked.connect(self.delete_elements)
        self.create_table()

    def create_table(self):
        result = self.db.get_categories()
        self.category_table.setColumnCount(1)
        self.category_table.setRowCount(len(result))
        for i, el in enumerate(result):
            self.category_table.setItem(i, 0, QTableWidgetItem(el))

    def delete_elements(self):
        rows = list(set([i.row() for i in self.category_table.selectedItems()]))
        categories = [self.category_table.item(i, 0).text() for i in rows]
        self.db.remove_category(*categories)
        self.create_table()
        self.parent.category_choosing()

    def create_category(self):
        name = str(self.name_of_category.text())
        if name.isdigit():
            mistake_messege('В названии категории нельзя использовать только цифры.')
        else:
            self.db.add_category(name)
            self.create_table()
            self.parent.category_choosing()

